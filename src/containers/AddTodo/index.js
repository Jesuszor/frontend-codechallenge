import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Style
import './style.scss';

class AddTodo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: ''
    };

    this._handleOnChange = this._handleOnChange.bind(this);
    this._handleOnSubmit = this._handleOnSubmit.bind(this);
  }

  render() {
    const { showingModal, handleClickOnAdd } = this.props;

    return (
      <div id="add-modal" className={showingModal ? 'showing' : ''}>
        <form onSubmit={this._handleOnSubmit}>
          <div>
            <label htmlFor="title">Title:</label>
            <input type="text" id="title" value={this.state.value} onChange={this._handleOnChange}/>
          </div>

          <input type="submit" value="Add"/>
          <button onClick={handleClickOnAdd}>Close</button>
        </form>
      </div>
    );
  }

  // Cada vez que el valor del input del titutlo cambia, coge el valor
  // y se lo asigna al estado value.
  _handleOnChange(e) {
    const value = e.target.value;

    this.setState({ value });
  }

  // Una vez el formulario haya sido enviado, coge el estado value y crea
  // un nuevo objecto "to do" a partir de dicho valor. Y luego llama la función
  // handleOnAdd que viene como props desde el componente padre y es la que 
  // se encarga de agregar el nuevo "to do". Todos los "to do's"
  // tienen una propiedad cuyo valor completed es false por defecto.
  _handleOnSubmit(e) {
    e.preventDefault();
    
    const todo = Object.assign({}, { title: this.state.value });
    todo.completed = false;

    this.setState({ value: '' });

    this.props.handleOnAdd(e, todo);
  }
}

AddTodo.propTypes = {
  handleOnAdd: PropTypes.func.isRequired,
  showingModal: PropTypes.bool.isRequired,
  handleClickOnAdd: PropTypes.func.isRequired
};

export default AddTodo;