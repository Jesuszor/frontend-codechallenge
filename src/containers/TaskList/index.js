import React, { Component } from 'react';

// Containers
import AddTodo from '../AddTodo';

// Components
import Loader from       '../../components/Loader';
import SingleTodo from   '../../components/SingleTodo';
import SearchBar from    '../../components/SearchBar';
import ErrorMessage from '../../components/ErrorMessage';

export default class TaskList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      todos: [],
      error: '',
      searchTerm: '',
      showingModal: false
    };

    this._handleOnChange   = this._handleOnChange.bind(this);
    this._handleOnSearch   = this._handleOnSearch.bind(this);
    this._handleOnDelete   = this._handleOnDelete.bind(this);
    this._handleClickOnAdd = this._handleClickOnAdd.bind(this);
    this._handleOnAdd      = this._handleOnAdd.bind(this);
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.state.todos.length !== nextState.todos.length || this.state.searchTerm !== nextState.searchTerm || this.state.error !== nextState.error;
  // }

  // Cada vez que el componente se va a montar hace un fetch
  // y obtiene la lista de todos los todos
  componentWillMount() {
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then((data) => {
        return data.json();
      })
      .then((jsonData) => {
        this.setState({ todos: jsonData, loading: false });
      })
      .catch((err) => {
        this.setState({ error: err.message, loading: false });
      });
  }

  // Si el estado de loading es verdadero, significa que el fetch todavía
  // no ha terminado, por lo tanto retorna el Loader.
  // Si hay algún error, mostrará el error.
  // Y si todo va bien, ejecutará el último return y mostrará toda la lista de todos
  render() {
    if (this.state.loading) return <Loader/>;
    if (this.state.error)   return <ErrorMessage error={this.state.error}/>
    
    return (
      <div id="todos" className="container">
        <div id="todos-container">
          <SearchBar handleOnSearch={this._handleOnSearch}/>

          <button onClick={this._handleClickOnAdd}>Add To do</button>
          {this.state.showingModal ? <AddTodo showingModal={this.state.showingModal} handleClickOnAdd={this._handleClickOnAdd} handleOnAdd={this._handleOnAdd}/> : ''}

          <h1>To do's</h1>
          <ul>
            {this._renderTodos(this.state.todos)}
          </ul>
        </div>
      </div>
    );
  }

  // Filtra la lista de "to do's" y busca los que contienen el término de la búsqueda
  // luego hace un map en cada elemento retornado y crea un componente por cada uno.
  _renderTodos(todos) {
    const todosList = todos.filter((todo) => {
      return todo.title.indexOf(this.state.searchTerm) > -1;
    }).map((todo, index) => {
      return (
        <SingleTodo
          todo={todo}
          key={index}
          index={index}
          handleOnChange={this._handleOnChange}
          handleOnDelete={this._handleOnDelete}
        />
      );
    });

    // Si la lista de todosList retornada es menor a 0 significa que no hay ningun
    // todo que contenga el texto introducido en la barra de búsqueda
    // así que retornamos que ningún todo ha sido encontrado, si es mayor a 0
    // significa que si hay uno o más todos que contengan el término de la búsqueda
    // y retornamos todos los componentes creados.
    return (todosList.length > 0) ? todosList : <li className="single-todo"><h2>No todos where found</h2></li>;
  }


  // Cada vez que se hace click en el checkbox, esta función se ejecuta.
  // Hacemos una copia de todos los todos, cambiamos el valor de la
  // propiedad completed del todo que se encuentra en el index correcto
  // y luego cambiamos el estado de todos los todos con la nueva copia
  // que hemos hecho.
  _handleOnChange(e, index) {
    const todos = [...this.state.todos];
    todos[index].completed = !todos[index].completed;

    this.setState({ todos });
  }

  // Cada vez que el valor del input cambia, esta función se ejecuta
  // y cambia el estado de searchTerm, lo que hace que el componente
  // se renderize y ejecute _renderTodos de nuevo, lo que hace un filtro
  // en cada uno de los todos con el término de la búsqueda introducido
  // en la barra de búsqueda.
  _handleOnSearch(e) {
    const text  = e.target.value;

    this.setState({ searchTerm: text });
  }

  // Cuando se hace click en el botón de borrar un todo se le pasa el index
  // del todo que se desea borrar, hacemos una copia de todos los todos,
  // luego hacemos un splice usando el index que recibimos como parámetro
  // para eliminar dicho todo de la copia que hemos realizado y por último
  // cambiamos el estado de los todos por el nuevo estado lo que hará
  // que se renderice de nuevo el componente y se llame la funcion _renderTodos. 
  _handleOnDelete(index) {
    let todos = [...this.state.todos];
    todos.splice(index, 1)

    this.setState({ todos });
  }

  // Cambia el estado de showingModal a true o false, esto hace que el modal se muestre
  // o no.
  _handleClickOnAdd(e) {
    e.preventDefault();

    this.setState({ showingModal: !this.state.showingModal });
  }

  // Esta función recibe los datos del formulario que se rellena en el modal que 
  // se abre cuando se hace click en el botón de agregar nuevo "to do", luego
  // le agrega un id y un userId a esos datos, crea un nuevo array con dicha data
  // como el primer elemento del array y el resto de elementos que existian previamente
  // en el estado de "to do's" y por último reemplaza el estado anterior por el nuevo
  // estado.
  _handleOnAdd(e, data) {
    e.preventDefault();

    if (data.title) {
      data.id = this.state.todos.length + 1;
      data.userId = this.state.todos[this.state.todos.length-1].userId + 1;

      const todos = [data, ...this.state.todos];

      this.setState({ todos, showingModal: false });
    }
  }
}