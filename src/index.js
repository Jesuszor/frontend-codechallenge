import React from 'react';
import { render } from 'react-dom';

// Style
import './styles/style.scss';

// Containers
import TaskList from './containers/TaskList';

// Main App
const app = document.getElementById('app');

render(
  <TaskList/>,
  app
);