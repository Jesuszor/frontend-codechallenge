import React from 'react';
import PropTypes from 'prop-types';

// Style
import './style.scss';

const SingleTodo = ({ todo, index, handleOnChange, handleOnDelete }) => {
  return (
    <li className={`single-todo ${todo.completed ? 'completed' : ''}`}>
      <input id={`todo${index}`} type="checkbox" checked={todo.completed} onChange={(e) => handleOnChange(e, index)}/>
      <label htmlFor={`todo${index}`}>{todo.title}</label>

      <button onClick={() => handleOnDelete(index)}>Delete</button>
    </li>
  );
};

SingleTodo.propTypes = {
  todo: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  handleOnChange: PropTypes.func.isRequired,
  handleOnDelete: PropTypes.func.isRequired
};

export default SingleTodo;