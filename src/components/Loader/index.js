import React from 'react';

// Style
import './style.scss';

const Loader = () => {
  return (
    <div id="loader">
      <h1>Loading...</h1>
    </div>
  );
};

export default Loader;