import React from 'react';
import PropTypes from 'prop-types';

// Style
import './style.scss';

const ErrorMessage = ({ error }) => {
  return (
    <div id="error">
      <h2>{error}</h2>
    </div>
  );
};

ErrorMessage.propTypes = {
  error: PropTypes.string.isRequired
};

export default ErrorMessage;