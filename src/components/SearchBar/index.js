import React from 'react';
import PropTypes from 'prop-types';

// Style
import './style.scss';

const SearchBar = ({ handleOnSearch }) => {
  return (
    <div className="search-bar">
      <h2>Search</h2>
      <input type="text" placeholder="Search todos" onChange={handleOnSearch}/>
    </div>
  );
};

SearchBar.propTypes = {
  handleOnSearch: PropTypes.func.isRequired
};

export default SearchBar;