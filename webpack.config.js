const webpack                  = require('webpack');
const path                     = require('path');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin        = require('html-webpack-plugin');
const OpenBrowserWebpackPlugin = require('open-browser-webpack-plugin');

const HtmlWebpack = new HtmlWebpackPlugin({
  template: path.join(__dirname + '/src/index.html'),
  inject: 'body',
  minify: {
    collapseWhitespace: true,
    collapseInlineTagWhitespace: true,
    removeComments: true,
    removeRedundantAttributes: true
  }
});
const ExtractText = new ExtractTextWebpackPlugin('style.css');
const OpenBrowser = new OpenBrowserWebpackPlugin({
  url: 'http://localhost:5000'
});
const OptimizeChunks = new webpack.optimize.CommonsChunkPlugin({
  name: 'vendor',
  filename: 'vendor.bundle.js'
});

module.exports = {
  entry: {
    vendor: ['react', 'react-dom', 'react-router-dom'],
    app: path.join(__dirname, 'src')
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-stage-0']
          }
        }
      },
      {
        test: /\.scss$/,
        exclude: /(node_modules)/,
        use: ExtractText.extract(['css-loader', 'sass-loader'])
      }
    ]
  },
  plugins: [HtmlWebpack, ExtractText, OpenBrowser, OptimizeChunks],
  devServer: {
    contentBase: path.join(__dirname + '/src'),
    port: 5000,
    compress: true
  }
};